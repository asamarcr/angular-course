import { PermissionGuard } from './core/guards/permission.guard';
import { LoggedGuard } from './core/guards/logged.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules, CanLoad, CanActivate } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { logging } from 'protractor';

const routes: Routes = [
  {
    path: '',
    canActivate: [LoggedGuard],
    loadChildren: (): Promise<any> =>
      import('./modules/autenticacion/autenticacion.module').then(
        (module) => module.AutenticacionModule
      ),
  },
  {
    path: 'home',
    canLoad: [AuthGuard],
    loadChildren: (): Promise<any> =>
      import('./modules/home/home.module').then(
        (module) => module.HomeModule
      ),
  },
  {
    path: 'administrador',
    canLoad: [AuthGuard],
    loadChildren: (): Promise<any> =>
      import('./modules/administrador/administrador.module').then(
        (module) => module.AdministradorModule
      ),
  },
  {
    path: 'dashboard',
    canLoad: [AuthGuard],
    loadChildren: (): Promise<any> =>
      import('./modules/dashboard/dashboard.module').then(
        (module) => module.DashboardModule
      ),
  },
  {
    path: 'datosabiertos',
    canLoad: [AuthGuard],
    loadChildren: (): Promise<any> =>
      import('./modules/datosabiertos/datosabiertos.module').then(
        (module) => module.DatosabiertosModule
      ),
  },
  {
    path: 'formularios',
    canLoad: [AuthGuard],
    loadChildren: (): Promise<any> =>
      import('./modules/formularios/formularios.module').then(
        (module) => module.FormulariosModule
      ),
  },
  {
  path: 'contactenos',
  canLoad: [AuthGuard],
  loadChildren: (): Promise<any> =>
    import('./modules/contactenos/contactenos.module').then(
      (module) => module.ContactenosModule
    ),
},
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
