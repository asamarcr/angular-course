import { Injectable } from '@angular/core';
import { Router, Route, CanLoad } from '@angular/router';
import { AutenticacionService } from '../services/autenticacion/autenticacion.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanLoad {
  constructor(
    private readonly autenticacionService: AutenticacionService,
    private readonly router: Router
  ) {}

  public canLoad(route: Route): boolean {
    if (!this.autenticacionService.isLoggedIn()) {
      this.router.navigate(['/login']);
    }
    return this.autenticacionService.isLoggedIn();
  }
}
