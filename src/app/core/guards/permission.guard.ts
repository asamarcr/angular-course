import { Injectable } from '@angular/core';
import {
  CanLoad,
  Route,
  UrlSegment
} from '@angular/router';
import { Observable } from 'rxjs';
import { AutenticacionService } from '../services/autenticacion/autenticacion.service';

@Injectable({
  providedIn: 'root',
})
export class PermissionGuard implements CanLoad {
  constructor(private readonly autenticacionService: AutenticacionService) {}
  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    const currentRole = this.autenticacionService.getLoggedUser().roles;

    if (currentRole === route.data.roleNeeded) {
      return true;
    }

    return false;
  }
}
