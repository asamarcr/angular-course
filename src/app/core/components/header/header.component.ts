import { Component, OnInit } from '@angular/core';
import { EventsHubService } from '../../services/events-hub/events-hub.service';
import { AutenticacionService } from '../../services/autenticacion/autenticacion.service';
import { Router } from '@angular/router';
import { PermissionsService } from '../../services/permissions/permissions.service';
import { faDashcube } from '@fortawesome/free-brands-svg-icons';
import { faChartLine, faListAlt, faUsers } from '@fortawesome/free-solid-svg-icons';

declare var $:any;

@Component({
  selector: 'mp-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public isLoggedIn: boolean;

  public dashboardIcon = faChartLine;
  public studentsIcon = faUsers;
  public courseIcon = faListAlt;

  constructor(
    private evenstHubService: EventsHubService,
    private autenticacionService: AutenticacionService,
    public permissionsService: PermissionsService
  ) {}

  ngOnInit(): void {
    this.isLoggedIn = false;

    this.evenstHubService.loggedIn$.subscribe((value) => {
      this.isLoggedIn = value;
    });

    // ---------Responsive-navbar-active-animation-----------
    function test(){
      var tabsNewAnim = $('#navbarSupportedContent');
      var selectorNewAnim = $('#navbarSupportedContent').find('li').length;
      var activeItemNewAnim = tabsNewAnim.find('.active');
      var activeWidthNewAnimHeight = activeItemNewAnim.innerHeight();
      var activeWidthNewAnimWidth = activeItemNewAnim.innerWidth();
      var itemPosNewAnimTop = activeItemNewAnim.position();
      var itemPosNewAnimLeft = activeItemNewAnim.position();
      $(".hori-selector").css({
        "top":itemPosNewAnimTop.top + "px", 
        "left":itemPosNewAnimLeft.left + "px",
        "height": activeWidthNewAnimHeight + "px",
        "width": activeWidthNewAnimWidth + "px"
      });
      $("#navbarSupportedContent").on("click","li",function(e){
        $('#navbarSupportedContent ul li').removeClass("active");
        $(this).addClass('active');
        var activeWidthNewAnimHeight = $(this).innerHeight();
        var activeWidthNewAnimWidth = $(this).innerWidth();
        var itemPosNewAnimTop = $(this).position();
        var itemPosNewAnimLeft = $(this).position();
        $(".hori-selector").css({
          "top":itemPosNewAnimTop.top + "px", 
          "left":itemPosNewAnimLeft.left + "px",
          "height": activeWidthNewAnimHeight + "px",
          "width": activeWidthNewAnimWidth + "px"
        });
      });
    }
    $(document).ready(function(){
      setTimeout(function(){ test(); });
    });
    $(window).on('resize', function(){
      setTimeout(function(){ test(); }, 500);
    });
    $(".navbar-toggler").click(function(){
      setTimeout(function(){ test(); });
    });
  
  }

  public logout(): void {

    this.autenticacionService.logout();
  }  
}
