import { Injectable } from '@angular/core';
import { AutenticacionService } from '../autenticacion/autenticacion.service';

@Injectable({
  providedIn: 'root',
})
export class PermissionsService {
  constructor(private readonly autenticacionService: AutenticacionService) {}

  public hasRole(role: string): boolean {
    const userRole = this.autenticacionService.getLoggedUser().roles;

    return role === userRole;
  }
}
