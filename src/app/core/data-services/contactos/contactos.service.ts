import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/app/config';
import { timeout } from 'rxjs/operators';
import { Contacto } from '../../../shared/models/contacto.model';

@Injectable({
  providedIn: 'root',
})
export class ContactosService {
  private apiPath = `${CONFIG.apiPath}/contactos`;

  constructor(private readonly http: HttpClient) {}

  public getContactos(): Observable<any> {
    return this.http.get(this.apiPath).pipe(timeout(CONFIG.timeoutRequest));
  }

  public getContacto(contactoId: number): Observable<any> {
    return this.http
      .get(`${this.apiPath}/${contactoId}`)
      .pipe(timeout(CONFIG.timeoutRequest));
  }

  public createContacto(body: Contacto ): Observable<any> {
    return this.http
      .post(`${this.apiPath}`, body)
      .pipe(timeout(CONFIG.timeoutRequest));
  }

  public editContacto(body: Contacto): Observable<any> {
    return this.http
      .put(`${this.apiPath}/${body.id}`, body)
      .pipe(timeout(CONFIG.timeoutRequest));
  }

  public deleteContacto(contactoId: number): Observable<any> {
    return this.http
      .delete(`${this.apiPath}/${contactoId}`)
      .pipe(timeout(CONFIG.timeoutRequest));
  }
}
