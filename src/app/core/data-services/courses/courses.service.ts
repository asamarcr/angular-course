import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/app/config';
import { timeout } from 'rxjs/operators';
import { Course } from '../../../shared/models/course.model';

@Injectable({
  providedIn: 'root',
})
export class CoursesService {
  private apiPath = `${CONFIG.apiPath}/courses`;

  constructor(private readonly http: HttpClient) {}

  public getCourses(): Observable<any> {
    return this.http.get(this.apiPath).pipe(timeout(CONFIG.timeoutRequest));
  }

  public getCourse(courseId: number): Observable<any> {
    return this.http
      .get(`${this.apiPath}/${courseId}`)
      .pipe(timeout(CONFIG.timeoutRequest));
  }

  public createCourse(body: Course ): Observable<any> {
    return this.http
      .post(`${this.apiPath}`, body)
      .pipe(timeout(CONFIG.timeoutRequest));
  }

  public editCourse(body: Course): Observable<any> {
    return this.http
      .put(`${this.apiPath}/${body.id}`, body)
      .pipe(timeout(CONFIG.timeoutRequest));
  }

  public deleteCourse(courseId: number): Observable<any> {
    return this.http
      .delete(`${this.apiPath}/${courseId}`)
      .pipe(timeout(CONFIG.timeoutRequest));
  }
}
