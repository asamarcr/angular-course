import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from '@rinminase/ng-charts';
import { TranslateModule } from '@ngx-translate/core';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbDatepickerModule, NgbNavModule, NgbPopoverModule, } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TranslateModule,
    SweetAlert2Module,
    ChartsModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    NgbDatepickerModule,
    NgbNavModule,
    NgbPopoverModule,
    FontAwesomeModule
  ],
  exports: [
    CommonModule,
    TranslateModule,
    SweetAlert2Module,
    ChartsModule,
    ReactiveFormsModule,
    FormsModule,
    NgbDatepickerModule,
    NgbNavModule,
    NgbPopoverModule,
    FontAwesomeModule
  ]
})
export class SharedModule { }
