import { Component, OnInit } from '@angular/core';
import { AutenticacionService } from '../../../core/services/autenticacion/autenticacion.service';

@Component({
  selector: 'mp-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public chartLegend = true;
  public chartLabels = [];
  public chartData = [];
  public active = 1;

  public user;

  constructor(
    private readonly autenticacionService: AutenticacionService
  ) {}

  ngOnInit(): void {
    this.loadChartData();
    this.getUser();
  }

  private getUser(): void{
    this.user = this.autenticacionService.getLoggedUser();
  }
  private loadChartData(): void {
    this.chartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    this.chartData = [
      { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
      { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
    ];
  }
}
