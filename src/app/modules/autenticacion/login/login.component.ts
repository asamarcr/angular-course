import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SecurityService } from '../../../core/data-services/security/security.service';
import { AutenticacionService } from '../../../core/services/autenticacion/autenticacion.service';
import { Credentials } from '../../../shared/models/credencials.module';

@Component({
  selector: 'mp-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public credentials: Credentials = new Credentials();
  public errorMsg: string = '';
  public submitted: boolean;

  constructor(
    private readonly autenticacionService: AutenticacionService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {}

  public login(form) {
    this.submitted = true;
    this.errorMsg = '';
    if (!form.valid) {
      return;
    }

    this.autenticacionService
      .login(this.credentials)
      .then(() => {
        this.router.navigateByUrl('/home');
      })
      .catch((error) => {
        this.errorMsg = error.message;
      });
  }
}
