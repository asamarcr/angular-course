import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultaPendienteComponent } from './consulta-pendiente/consulta-pendiente.component';

const routes: Routes = [  {
  path: '',
  component: ConsultaPendienteComponent,
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatosabiertosRoutingModule { }
