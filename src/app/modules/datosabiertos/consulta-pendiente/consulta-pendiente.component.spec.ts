import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaPendienteComponent } from './consulta-pendiente.component';

describe('ConsultaPendienteComponent', () => {
  let component: ConsultaPendienteComponent;
  let fixture: ComponentFixture<ConsultaPendienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaPendienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaPendienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
