import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DatosabiertosRoutingModule } from './datosabiertos-routing.module';
import { ConsultaPendienteComponent } from './consulta-pendiente/consulta-pendiente.component';


@NgModule({
  declarations: [ConsultaPendienteComponent],
  imports: [
    CommonModule,
    DatosabiertosRoutingModule
  ]
})
export class DatosabiertosModule { }
