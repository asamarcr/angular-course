import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministradorModalComponent } from './administrador-modal.component';

describe('AdministradorModalComponent', () => {
  let component: AdministradorModalComponent;
  let fixture: ComponentFixture<AdministradorModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministradorModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministradorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
