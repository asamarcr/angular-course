import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {
  faCalendar,
  faCalendarTimes,
} from '@fortawesome/free-regular-svg-icons';
import { faCalendarDay } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Course } from 'src/app/shared/models/course.model';
import { CoursesService } from '../../../core/data-services/courses/courses.service';
import { AbstractForm } from '../../../shared/components/abstracts/abstract-form';

@Component({
  selector: 'mp-administrador-modal',
  templateUrl: './administrador-modal.component.html',
  styleUrls: ['./administrador-modal.component.scss'],
})
export class AdministradorModalComponent extends AbstractForm implements OnInit {
  @Input() public course: Course;

  @Output()
  public readonly successfulTransaction = new EventEmitter<boolean>();

  public calendarIcon = faCalendarDay;

  constructor(
    private modalService: NgbModal,
    private toastr: ToastrService,
    private readonly coursesService: CoursesService,
    public formBuilder: FormBuilder
  ) {
    super(formBuilder);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  public isEditMode(): boolean {
    return !!this.course?.id;
  }

  public get modalTitle(): string {
    return this.isEditMode() ? 'Edit Course' : 'Create Course';
  }
  public get modalAction(): string {
    return this.isEditMode() ? 'Edit Course' : 'Create Course';
  }

  protected initForm(): void {
    this.form = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(5)]],
      author: [, [Validators.required]]
    });

    if (this.isEditMode()) {
      this.form.patchValue(this.course);
    }
  }

  public closeModal(): void {
    this.modalService.dismissAll();
  }

  public submitHandler() {
    this.submitAttempt = true;
    if (!this.form.valid) {
      return;
    }

    const action = this.isEditMode()
      ? this.coursesService.editCourse({...this.form.value, id: this.course.id})
      : this.coursesService.createCourse(this.form.value);

    action.subscribe(() => {
      const message = this.isEditMode() ? 'Course Edited Successfully' : 'Course Created Successfully'
      this.toastr.success(message);
      this.successfulTransaction.emit(true);
      this.closeModal();
    });
  }
}
