import { Component, OnInit } from '@angular/core';
import { CoursesService } from '../../../core/data-services/courses/courses.service';
import { ContactosService } from '../../../core/data-services/contactos/contactos.service';
import { Course } from '../../../shared/models/course.model';
import { Contacto } from '../../../shared/models/contacto.model';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdministradorModalComponent } from '../administrador-modal/administrador-modal.component';


@Component({
  selector: 'mp-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.scss']
})
export class AdministradorComponent implements OnInit {
  
  //public list: Course[];
  public list: Contacto[];
  public loading: boolean;

  constructor(
    private readonly courseService: CoursesService,
    private readonly contactoService: ContactosService,
    private readonly toastr: ToastrService,
    private readonly modalService: NgbModal
  ) {}

  ngOnInit(): void {
    //this.loadCourses();
    this.loadContactos();
  }

  private loadCourses(): void {
    this.loading = true;
    this.courseService.getCourses().subscribe(
      (result) => {
        setTimeout(() => {
          this.list = result;
          console.log(this.list);
          this.loading = false;
        }, 1000);
      },
      (error) => {
        this.toastr.error(error);
      }
    );
  }
  private loadContactos(): void {
    this.loading = true;
    this.contactoService.getContactos().subscribe(
      (result) => {
        setTimeout(() => {
          this.list = result;
          console.log(this.list);
          this.loading = false;
        }, 1000);
      },
      (error) => {
        this.toastr.error(error);
      }
    );
  }
  public deleteCourse(course: Course): void {
    this.courseService.deleteCourse(course.id).subscribe(() => {
      this.toastr.success('Course Deleted successfully');
      this.loadCourses();
    });
  }
  public deleteContacto(contacto: Contacto): void {
    this.contactoService.deleteContacto(contacto.id).subscribe(() => {
      this.toastr.success('Contacto Deleted successfully');
      this.loadContactos();
    });
  }

  public createCourse(): void {
    const modalRef = this.modalService.open(AdministradorModalComponent, {
      size: 'md',
      centered: true,
    });

    modalRef.componentInstance.successfulTransaction.subscribe(() => {
      this.loadCourses();
    });
  }

  public createContacto(): void {
    const modalRef = this.modalService.open(AdministradorModalComponent, {
      size: 'md',
      centered: true,
    });

    modalRef.componentInstance.successfulTransaction.subscribe(() => {
      this.loadContactos();
    });
  }

  public editCourse(course: Course): void {
    const modalRef = this.modalService.open(AdministradorModalComponent, {
      size: 'md',
      centered: true,
    });

    modalRef.componentInstance.course = course;

    modalRef.componentInstance.successfulTransaction.subscribe(() => {
      this.loadCourses();
    });
  }

  public editContacto(contacto: Contacto): void {
    const modalRef = this.modalService.open(AdministradorModalComponent, {
      size: 'md',
      centered: true,
    });

    modalRef.componentInstance.contacto = contacto;

    modalRef.componentInstance.successfulTransaction.subscribe(() => {
      this.loadContactos();
    });
  }
}



