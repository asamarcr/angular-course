import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministradorRoutingModule } from './administrador-routing.module';
import { AdministradorComponent } from './administrador/administrador.component';
import { AdministradorModalComponent } from './administrador-modal/administrador-modal.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [AdministradorComponent, AdministradorModalComponent ],
  imports: [
    AdministradorRoutingModule,
    SharedModule
  ]
})
export class AdministradorModule { }
