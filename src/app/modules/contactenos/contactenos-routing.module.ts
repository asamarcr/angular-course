import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactoMuniComponent } from './contacto-muni/contacto-muni.component';

const routes: Routes = [  {
  path: '',
  component:ContactoMuniComponent,
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactenosRoutingModule { }
