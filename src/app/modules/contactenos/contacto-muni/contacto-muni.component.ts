import { Component, EventEmitter, Input, OnInit, Output, NgZone } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AbstractForm } from '../../../shared/components/abstracts/abstract-form';
import { ToastrService } from 'ngx-toastr';
import { Contacto } from 'src/app/shared/models/contacto.model';
import { ContactosService } from '../../../core/data-services/contactos/contactos.service';


@Component({
  selector: 'mp-contacto-muni',
  templateUrl: './contacto-muni.component.html',
  styleUrls: ['./contacto-muni.component.scss']
})
export class ContactoMuniComponent extends AbstractForm implements OnInit {

  @Input() public contacto: Contacto;

  @Output()

  public readonly successfulTransaction = new EventEmitter<boolean>();
  constructor(
    private readonly contactosService: ContactosService,    
    private toastr: ToastrService,
    public formBuilder: FormBuilder
  ) {
    super(formBuilder);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  public isEditMode(): boolean {
    return !!this.contacto?.id;
  }

 protected initForm(): void {
    this.form = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      email: [, [Validators.required]],
      telefono: ['', [Validators.required]],
      mensaje: ['', [Validators.required]],
    });
  }
 
  public submitHandler() {
    this.submitAttempt = true;
    if (!this.form.valid) {
      return;
    }

    const action = this.isEditMode()
      ? this.contactosService.editContacto({...this.form.value, id: this.contacto.id})
      : this.contactosService.createContacto(this.form.value);

    action.subscribe(() => {
      const message = this.isEditMode() ? 'Course Edited Successfully' : 'Data Created Successfully'
      this.toastr.success(message);
      this.successfulTransaction.emit(true);
      window.location.reload();
    });
  }
}
