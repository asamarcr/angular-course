import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactoMuniComponent } from './contacto-muni.component';

describe('ContactoMuniComponent', () => {
  let component: ContactoMuniComponent;
  let fixture: ComponentFixture<ContactoMuniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactoMuniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactoMuniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
