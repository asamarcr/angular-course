import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactenosRoutingModule } from './contactenos-routing.module';
import { ContactoMuniComponent } from './contacto-muni/contacto-muni.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [ContactoMuniComponent],
  imports: [
    CommonModule,
    ContactenosRoutingModule,
    SharedModule
  ]
})
export class ContactenosModule { }
