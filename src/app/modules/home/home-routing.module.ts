import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaginaHomeComponent } from './pagina-home/pagina-home.component';

const routes: Routes = [

  {
    path: '',
    component: PaginaHomeComponent,
  },
 ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
